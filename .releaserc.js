module.exports = {
    "plugins": [
        ["@semantic-release/commit-analyzer", {
            "releaseRules": [
                {"type": "build", "release": "patch"},
                {"type": "ci", "release": "patch"},
                {"type": "docs", "release": "patch"},
                {"type": "refactor", "release": "patch"},
                {"type": 'feat', "release": 'minor'},
                {"type": 'fix', "release": 'patch'},
                {"type": 'perf', "release": 'patch'},
                {"type": 'chore', "release": 'patch'}
            ]
        }],
        "@semantic-release/npm",
        "@semantic-release/release-notes-generator",
        "@semantic-release/gitlab",
        ["@semantic-release/changelog", {
            "changelogFile": "CHANGELOG.md"
        }],
        ["@semantic-release/git", {
            "assets": ["CHANGELOG.md", "package.json"]
        }],
        ["@semantic-release/exec", {
            "verifyReleaseCmd": "echo ${nextRelease.version} > .VERSION"
        }]
    ],
    "branches": [
        "main"
    ]
}
