image: node:16-alpine3.11

stages:
  - prepare
  - test
  - version
  - release
  - deploy

# Include pipeline templates
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - 'gitlab-ci-templates/registry-deploy/.gitlab-ci-inherit.yml'
  - 'gitlab-ci-templates/registry-deploy/php-8.1/*'

# -------------------- #
# GLOBAL SHARED CONFIG
# -------------------- #
.pull-only-cache-npm:
  before_script:
    - echo "Displaying installed npm packages:"
    - npm list --depth 0 --package-lock-only
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - node_modules/
    policy: pull

# ------------------ #
# DEPENDENCIES
# ------------------ #
install npm dependencies:
  stage: prepare
  needs: []
  script:
    - npm install
  after_script:
    - npm list --depth 0 --package-lock-only
  cache:
    policy: pull-push
    key:
      files:
        - package-lock.json
    paths:
      - node_modules/
  artifacts:
    paths:
      - public
    expire_in: 1 day

# ------------------ #
# TESTING
# ------------------ #
test lint commit:
  stage: test
  extends: .pull-only-cache-npm
  needs:
    - install npm dependencies
  before_script:
    - apk add --no-cache git
  script:
    - echo "$CI_COMMIT_TITLE" | npx --no-install commitlint --verbose

# ------------------ #
# RELEASE
# ------------------ #
get next release:
  stage: version
  extends: .pull-only-cache-npm
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
  before_script:
    - apk add --no-cache git
  script:
    - npx semantic-release --dry-run --no-ci
    - test -e .VERSION || (echo $(git describe --abbrev=0 --tags | tr -d v) > .VERSION && touch .NORELEASE)
    - echo "Determined Version $(cat .VERSION)"
  artifacts:
    paths:
      - .VERSION
      - .NORELEASE
    expire_in: 1 week

release:
  stage: release
  extends: .pull-only-cache-npm
  needs:
    - install npm dependencies
    - test lint commit
    - get next release
  when: manual
  rules:
    - if: $GL_TOKEN && $CI_COMMIT_BRANCH == "main"
  before_script:
    - apk add --no-cache git
  script:
    - npx semantic-release

# ------------------ #
# DEPLOY
# ------------------ #
# Deployment .gitlab-ci.yml files are located in gitlab-ci-templates