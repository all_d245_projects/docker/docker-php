## [1.1.4](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.1.3...v1.1.4) (2022-03-24)

## [1.1.3](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.1.2...v1.1.3) (2022-03-24)

## [1.1.2](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.1.1...v1.1.2) (2022-03-24)

## [1.1.1](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.1.0...v1.1.1) (2022-03-24)

# [1.1.0](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.0.2...v1.1.0) (2022-03-24)


### Features

* docker image modifications [[#4](https://gitlab.com/all_d245_projects/docker/docker-php/issues/4)] ([681e56b](https://gitlab.com/all_d245_projects/docker/docker-php/commit/681e56b751728bae0fcf071d9f908bec0d75c205))

## [1.0.2](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.0.1...v1.0.2) (2022-03-24)

## [1.0.1](https://gitlab.com/all_d245_projects/docker/docker-php/compare/v1.0.0...v1.0.1) (2022-03-24)

# 1.0.0 (2022-03-24)


### Features

* build pipeline for registry deploy and release [[#1](https://gitlab.com/all_d245_projects/docker/docker-php/issues/1)] ([f8efea0](https://gitlab.com/all_d245_projects/docker/docker-php/commit/f8efea02a63e20af327a75c161393e6222296b2c))
* build pipeline for registry deploy and release [[#1](https://gitlab.com/all_d245_projects/docker/docker-php/issues/1)] ([d212f53](https://gitlab.com/all_d245_projects/docker/docker-php/commit/d212f5387f56af11357f6d7d7c5809f64d86efa3))
* build pipeline for registry deploy and release [[#1](https://gitlab.com/all_d245_projects/docker/docker-php/issues/1)] ([8d5cbea](https://gitlab.com/all_d245_projects/docker/docker-php/commit/8d5cbeacda65fa662164c600a72c6773a3354504))
* build pipeline for registry deploy and release [[#1](https://gitlab.com/all_d245_projects/docker/docker-php/issues/1)] ([dc9010b](https://gitlab.com/all_d245_projects/docker/docker-php/commit/dc9010baed950555700075ae8057345187c08a77))
* build pipeline for registry deploy and release [[#1](https://gitlab.com/all_d245_projects/docker/docker-php/issues/1)] ([c7b71d1](https://gitlab.com/all_d245_projects/docker/docker-php/commit/c7b71d125f7fc88433cd9f33d9aa0ffa27beb6bd))
